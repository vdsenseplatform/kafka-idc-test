## Kafka connection:
Brokers: 171.244.7.230:9092
Topic: test.motion

## Tạo event
Để chạy script tạo event, cần NodeJS >= 8 + Yarn

```
yarn
node emit-motion-event.js
```

Script sẽ sinh 1 event/s. Ctrl+C để tắt script.
