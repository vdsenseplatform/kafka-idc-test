#!/usr/bin/env node

function sleep(millis) {
  return new Promise(resolve => setTimeout(resolve, millis));
}

const { Kafka } = require('kafkajs');

async function main() {
  const kafka = new Kafka({
    clientId: 'test',
    brokers: ['171.244.7.230:9092']
  });

  const producer = kafka.producer();

  await producer.connect();
  console.info('Connected')

  while (true) {
    const payload = {
      stream_url: "rtmp://example",  // Original stream url
      timestamp_ms: new Date().getTime(),  // Timestamp in milliseconds
      score: 0.81  // Confidence
    };
    console.info(payload);

    await producer.send({
      topic: 'test.motion',
      messages: [{
        value: JSON.stringify(payload),
        key: "rtmp://example"
      }]
    });

    await sleep(1000);
  }
}

main().catch(console.error)
